import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { ListerPersonnesComponent } from './lister-personnes/lister-personnes.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { DetailPersonneComponent } from './detail-personne/detail-personne.component';
import { AjouterPersonneComponent } from './ajouter-personne/ajouter-personne.component';
import {FormsModule} from '@angular/forms';
import { SplashComponent } from './splash/splash.component';
import { RecherchePersonneComponent } from './recherche-personne/recherche-personne.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {ApiCarnetBrokerService} from './api-carnet-broker.service';

const appRoutes: Routes = [
  // 1 route par module
  { path: 'splash', component: SplashComponent},
  { path: 'ajouter', component: AjouterPersonneComponent},
  { path: 'lister', component: ListerPersonnesComponent},
  { path: 'rechercher', component: RecherchePersonneComponent},
  { path: 'details/:id', component: DetailPersonneComponent},
  { path: '', component: SplashComponent},
  { path: '**', component: SplashComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ListerPersonnesComponent,
    DetailPersonneComponent,
    AjouterPersonneComponent,
    SplashComponent,
    RecherchePersonneComponent
  ],
  imports: [
    BrowserModule, NgbModule, FontAwesomeModule, FormsModule, RouterModule.forRoot(appRoutes), HttpClientModule
  ],
  providers: [ApiCarnetBrokerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
