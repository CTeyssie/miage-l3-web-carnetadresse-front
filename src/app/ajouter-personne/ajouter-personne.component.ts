import { Component, OnInit } from '@angular/core';
import {Personne} from '../Personne';
import {ApiCarnetBrokerService} from '../api-carnet-broker.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-ajouter-personne',
  templateUrl: './ajouter-personne.component.html',
  styleUrls: ['./ajouter-personne.component.css']
})
export class AjouterPersonneComponent implements OnInit {

  personne: Personne;
  constructor(private apiCarnetBrokerService: ApiCarnetBrokerService,
              private httpClient: HttpClient) { }

  ngOnInit() {
    this.personne = new Personne();
  }

  valider() {
    this.apiCarnetBrokerService.ajouterPersonne(this.personne);
  }

}
