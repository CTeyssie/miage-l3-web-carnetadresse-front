import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Personne} from './Personne';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiCarnetBrokerService {

  private url = 'http://localhost:3000/api/personnes';

  constructor(private httpClient: HttpClient) { }

  public recupererliste(): Observable<Personne[]> {
    return this.httpClient.get<Personne[]>(this.url);
  }

  public getPersonne(id: number): Observable<Personne> {
    return this.httpClient.get<Personne>(this.url + '/' + id);
  }

  public ajouterPersonne(pers: Personne) {
    this.httpClient.post<Personne>(this.url, pers)
      .subscribe(
        (response) => {console.log(response);}
        , (error) => {console.log('Erreur ajouter')}
      )
  }
}
