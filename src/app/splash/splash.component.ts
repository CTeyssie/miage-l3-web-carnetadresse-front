import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.css']
})
export class SplashComponent implements OnInit {

  public lat = 0;
  public long = 0;
  constructor() { }

  ngOnInit() {
    this.getGeoLocation();
  }

  getGeoLocation() {
    console.log("toto");
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.long = position.coords.longitude;
        console.log(position);
      }, error => console.log(error));
    }
  }

}
