import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecherchePersonneComponent } from './recherche-personne.component';

describe('RecherchePersonneComponent', () => {
  let component: RecherchePersonneComponent;
  let fixture: ComponentFixture<RecherchePersonneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecherchePersonneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecherchePersonneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
