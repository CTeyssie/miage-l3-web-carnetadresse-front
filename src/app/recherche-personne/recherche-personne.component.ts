import { Component, OnInit } from '@angular/core';
import {faArrowAltCircleRight} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-recherche-personne',
  templateUrl: './recherche-personne.component.html',
  styleUrls: ['./recherche-personne.component.css']
})
export class RecherchePersonneComponent implements OnInit {

  faArrowAltCircleRight = faArrowAltCircleRight;
  id: number;
  constructor() { }

  ngOnInit() {
  }

}
