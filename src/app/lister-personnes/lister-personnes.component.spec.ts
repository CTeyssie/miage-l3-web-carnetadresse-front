import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListerPersonnesComponent } from './lister-personnes.component';

describe('ListerPersonnesComponent', () => {
  let component: ListerPersonnesComponent;
  let fixture: ComponentFixture<ListerPersonnesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListerPersonnesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListerPersonnesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
