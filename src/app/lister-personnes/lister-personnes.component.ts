import { Component, OnInit } from '@angular/core';
import {Personne} from '../Personne';
import {faSearchPlus} from '@fortawesome/free-solid-svg-icons';
import {ApiCarnetBrokerService} from '../api-carnet-broker.service';

@Component({
  selector: 'app-lister-personnes',
  templateUrl: './lister-personnes.component.html',
  styleUrls: ['./lister-personnes.component.css']
})
export class ListerPersonnesComponent implements OnInit {
  faSearchPlus = faSearchPlus;
  liste: Personne[] = [];

  constructor(private apiCarnetBrokerService: ApiCarnetBrokerService) {
  }
  ngOnInit() {
  this.apiCarnetBrokerService.recupererliste().subscribe((data) => {this.liste = data});
    /*
    const p1 = new Personne();
    p1.nom = 'Nom 1';
    p1.prenom = 'Prenom 1';
    p1.id = 1;
    p1.adresse = 'adr 1';
    p1.codepostal = 12345;
    p1.ville = 'Ville 1';
    p1.datemiseajour = '27-03-2020';
    this.liste.push(p1);
    const p2 = new Personne();
    p2.nom = 'Nom 2';
    p2.prenom = 'Prenom 2';
    p2.id = 2;
    p2.adresse = 'adr 2';
    p2.codepostal = 31000;
    p2.ville = 'Ville 2';
    p2.datemiseajour = '27-03-2020';
    this.liste.push(p2);

     */
  }

}
