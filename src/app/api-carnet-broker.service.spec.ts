import { TestBed } from '@angular/core/testing';

import { ApiCarnetBrokerService } from './api-carnet-broker.service';

describe('ApiCarnetBrokerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiCarnetBrokerService = TestBed.get(ApiCarnetBrokerService);
    expect(service).toBeTruthy();
  });
});
