import {Pipo} from './Pipo';

export class Personne {
  id: number;
  nom: string;
  prenom: string;
  adresse: string;
  codepostal: number;
  ville: string;
  datemiseajour: string;
  ratch: Pipo;
}
