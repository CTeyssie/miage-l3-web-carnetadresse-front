import { Component, OnInit } from '@angular/core';
import {Personne} from '../Personne';
import {ApiCarnetBrokerService} from '../api-carnet-broker.service';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-detail-personne',
  templateUrl: './detail-personne.component.html',
  styleUrls: ['./detail-personne.component.css']
})
export class DetailPersonneComponent implements OnInit {

  personne: Personne;
  constructor(private apiCarnetBrokerService: ApiCarnetBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeactive: ActivatedRoute) { }

  ngOnInit() {
    const id = this.routeactive.snapshot.params.id;
    this.apiCarnetBrokerService.getPersonne(id).subscribe((data) => { this.personne = data });
    /*
    this.personne = new Personne();
    this.personne.id = 1;
    this.personne.nom = 'Le nom de la pers';
    this.personne.prenom = 'Le prénom de la pers';
    this.personne.ville = 'La ville de la pers';
    this.personne.codepostal = 31000;
    this.personne.datemiseajour = '27-03-2020';


     */
  }

}
